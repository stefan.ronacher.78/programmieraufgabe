# Programmieraufgabe

Um das Projekt zu benutzen:

1. Ins Projektverzeichnis wechseln

2. Laravel Server starten: php artisan serve

3. Apache Tika Server starten:
- cd tika
- java -jar tika-server-1.26.jar

4. http://localhost:8000 im Webexplorer öffnen

---
Um den Test zu machen:

1. Ins Projektverzeichnis wechseln

2. Test starten: php artisan test

---
Hinweis: Die Grösse der gewählten Datei kann durch die Einstellungen von php.ini limitiert sein.
Um das zu ändern muss man php.ini editieren und einen passenden Wert bei folgenden Zeilen eingeben (Danach ist eventuell ein Restart des Rechners nötig):
- upload_max_filesize =
- post_max_size =
