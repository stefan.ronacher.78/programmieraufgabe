<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Http\Controllers;

class GetContentTest extends TestCase
{
    public function test1()
    {
      echo "1) Gebe existierenden Dateinamen an.  Die Funktion sollte einen String wiedergeben.\n";
      $file["tmp_name"] = "public/test_files/Curriculum deutsch.pdf";
      $file["name"] = "test_files/Curriculum deutsch.pdf";
      $root = new \App\Http\Controllers\Root();
      $test_result = $root->get_content($file);

      $this->assertNotEquals($test_result, "");
    }

    public function test2()
    {
      echo "1) Gebe nichtexistierenden Dateinamen an.  Die Funktion sollte einen leeren String wiedergeben.\n";
      $file["tmp_name"] = "public/test_files/CCurriculum deutsch.pdf";
      $file["name"] = "test_files/CCurriculum deutsch.pdf";
      $root = new \App\Http\Controllers\Root();
      $test_result = $root->get_content($file);

      $this->assertEmpty($test_result, "");
    }
}
