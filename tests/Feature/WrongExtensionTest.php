<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Http\Controllers;

class WrongExtensionTest extends TestCase
{
    public function test1()
    {
      echo "1) Gebe Dateinamen mit ungültiger extension an (test.ppf).  Die Funktion sollte true wiedergeben.\n";
      $root = new \App\Http\Controllers\Root();
      $test_result = $root->invalid_extension("test.ppf");

      $this->assertTrue($test_result);
    }

    public function test2()
    {
      echo "2) Gebe Dateinamen mit gültiger extension an (test.pdf).  Die Funktion sollte false wiedergeben.";
      $root = new \App\Http\Controllers\Root();
      $test_result = $root->invalid_extension("test.pdf");

      $this->assertFalse($test_result);
    }
}
