<h3>Inhalt der hochgeladenen Datei << {{$file_name}} >>:</h3><br>
@if( !trim(strip_tags($content)) )
  Kein extrahierter Inhalt von dieser Datei.
@else
  {!!$content!!}
@endif
