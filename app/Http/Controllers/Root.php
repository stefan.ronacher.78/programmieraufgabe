<?php
namespace App\Http\Controllers;
use Vaites\ApacheTika\Client;
use Illuminate\Http\Request;
use App\Functions\Page;

class Root extends Controller
{
  /* Detects if file type is invalid
    Arguments:
      $file_name: String with file name
  */
  public function invalid_extension($file_name)
  {
    $valid_extensions = array("doc","docx","pdf");
    $filename_parts = pathinfo($file_name);
    if( isset($filename_parts['extension']) ) {
      $extension = strtolower($filename_parts['extension']);
      return !in_array($extension,$valid_extensions);
    }
    else
      return true;
  }

  /* returns template with content of given file
    Arguments:
      $file: extracted from $_FILES (POSTED from file field in form)
  */
  public function get_content($file)
  {
    //Initialize link to Tika server
    $client = \Vaites\ApacheTika\Client::make('http://localhost:9998');
    $client = \Vaites\ApacheTika\Client::prepare('http://localhost:9998');

    if( file_exists($file["tmp_name"]) ){
      //Get Data from file
      $full_content = $client->getHTML($file["tmp_name"]);
      //Just show between <body> and </body>
      preg_match("/<body[^>]*>(.*?)<\/body>/is", $full_content, $matches);
      //$data is sent to template file
      $data["content"] = $matches[1];
      $data["file_name"] = $file["name"];
      return $this->get_page("content","",$data);
    }
  }

  /* Processes given file
    Arguments:
      $file: extracted from $_FILES (POSTED from file field in form)
  */
  public function proc_file($file)
  {
    //If extension is invalid, error message and form is shown
    if( $this->invalid_extension($file["name"]) )
      return $this->get_page("form","<b><font color='red'>Nur .doc / .docx / .pdf Dateien</font></b>");
    else
      return $this->get_content($file);
  }

  /**
   * Handle the incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function __invoke(Request $request)
  {
    //If there is no file loaded, form is shown
    if( !isset($_FILES["file"]["name"]) )
      return $this->get_page("form");
    else
      return $this->proc_file($_FILES["file"]);
  }
}
?>
