<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
  use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

  public function get_page($uri,$message = "",$data=array()) {
    $ret = "";
    $ret .= view('includes/head');
    if( $message )
      $ret .= $message."<br>";
    $ret .= view($uri,$data);
    $ret .= view('includes/foot');
    return $ret;
  }
}
